__________________
Testing:
1) 2 true
2) 3 true
3) 4 true
4) 1024 true
5) 729 true
6) 150 true
7) 49 false

__________________
Solutions:
1) find remainders, beats 75.21 % 4ms fastest:
class Solution {
public:
    bool isUgly(int num) {
        if (num <= 0)
        {
            return false;
        }
        
        while (true)
        {
            if (num % 2 == 0)
            {
                num /= 2;
                continue;
            }
            else if (num % 3 == 0)
            {
                num /= 3;
                continue;
            }
            else if (num % 5 == 0)
            {
                num /= 5;
                continue;
            }
            else
            {
                break;
            }
        }
        
        return num == 1;
    }
};

__________________
Task:
Write a program to check whether a given number is an ugly number.

Ugly numbers are positive numbers whose prime factors only include 2, 3, 5. 
For example, 6, 8 are ugly while 14 is not ugly since it includes another prime factor 7.

Note that 1 is typically treated as an ugly number.