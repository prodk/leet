
1) A simple in-order traversal, beats 10.33%
/**
 * Definition for a binary tree node.
 * public class TreeNode {
 *     int val;
 *     TreeNode left;
 *     TreeNode right;
 *     TreeNode(int x) { val = x; }
 * }
 */
import java.lang.*;

public class Solution {
    public int maxDepth(TreeNode root) {
        if (null != root)
        {
            if ((null == root.left) && (null == root.right))
            {
                return 1;
            }
            
            int leftDepth = maxDepth(root.left);
            int rightDepth = maxDepth(root.right);
            
            return Math.max(leftDepth+1, rightDepth+1);
        }
        
        return 0;
    }
}