__________________
Testing:
0) "["
false
1) "[[[[[(]]]]])"
false
2) "[[[[[]]]]])"
false
3) "[[[[[]]]]]()"
true
4) "[[[[[]]]]](){}{}{}{{{{{{{{{{}}}}}}}}}}"
true
5) "]["
false
6) "()()()}{"
false
7) "(((([[[[[[[]]]]]]))))"
false
8) "([])"
true
9) "{{}}{{{[[[((()))]]]}}}"
true
10) "}"

__________________
Solutions:

1) another approach - recursive;

0) use a stack to record the current state, beats 15.4 %, 0 ms, fastest:
class Solution {
public:
    bool isValid(string s) {
        int rnd = 0;
        int curl = 0;
        int sq = 0;
        
        std::stack<char> state;
        
        const size_t len = s.size();
        for (size_t i = 0; i < len; ++i)
        {
            //std::cout << i << std::endl;
            if (!count(s[i], '(', ')', rnd, state))
            {
                return false;
            }
            
            if (!count(s[i], '{', '}', curl, state))
            {
                return false;
            }
            
            if (!count(s[i], '[', ']', sq, state))
            {
                return false;
            }
        }
        
        return rnd == 0 && curl == 0 && sq == 0;
    }
    
private:
    bool count(const char ch, const char chOpen, const char chClose, int & cnt, std::stack<char> & state) const
    {
        if (ch == chOpen)
        {
            ++cnt;
            state.push(chOpen);
        }
        else if (ch == chClose)
        {
            //std::cout << ch << std::endl;
            if (!state.empty() && state.top() != getState(ch))
            {
                //std::cout << "no" << std::cout;
                return false;
            }
            else
            {
                --cnt;
                if (!state.empty())
                {
                    //std::cout << "pop0" << std::endl;
                    state.pop();
                    //std::cout << "pop1" << std::endl;
                }
            }
            
            if (cnt < 0)
            {
                return false;
            }
        }
        
        return true;
    }
    
    char getState(const char ch) const
    {
        char res = '[';
        switch(ch)
        {
            case '[':
            case ']':
            return '[';
            
            case '(':
            case ')':
            return '(';
            
            case '{':
            case '}':
            return '{';
        }
        
        return res;
    }
};

__________________
Task:
20. Valid Parentheses

Given a string containing just the characters '(', ')', '{', '}', '[' and ']', determine if the input string is valid.

The brackets must close in the correct order, "()" and "()[]{}" are all valid but "(]" and "([)]" are not.


