__________________
Testing:
0) [1]
1) [1,1]
2) [1,2,1]
3) [1,3,3,1]
4) [1,4,6,4,1]
5) [1,5,10,10,5,1]
7) [1,7,21,35,35,21,7,1]
10)[1,10,45,120,210,252,210,120,45,10,1]
15)[1,15,105,455,1365,3003,5005,6435,6435,5005,3003,1365,455,105,15,1]
20)[1,20,190,1140,4845,15504,38760,77520,125970,167960,184756,167960,125970,77520,38760,15504,4845,1140,190,20,1]


__________________
Solutions:

0) keep 2 vectors: previous and current, beats 24.78 %, 0 ms, fastest:
class Solution {
public:
    vector<int> getRow(int rowIndex) {
        std::vector<int> res;
        res.push_back(1);
        if (rowIndex == 0)
        {
            return res;
        }
        res.push_back(1);
        if (rowIndex == 1)
        {
            return res;
        }
        
        std::vector<int> prev = res;
        
        for (int i = 2; i <= rowIndex; ++i)
        {
            res.resize(i + 1);
            res[res.size()-1] = 1;
            
            for (int j = 1; j < prev.size(); ++j)
            {
                res[j] = prev[j-1] + prev[j];
            }
            
            prev.swap(res);
        }
        
        return prev;
    }
};

__________________
Task:
Given an index k, return the kth row of the Pascal's triangle.

For example, given k = 3,
Return [1,3,3,1].

Note:
Could you optimize your algorithm to use only O(k) extra space?