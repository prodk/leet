__________________
Testing:
0) "  absd    "
4
1) ""
0
2) " abcdwwaAAB  ABDdlajeb    "
9
3) 
__________________
Solutions:

0) just go from the back, skip spaces and get the word, beats %, 1 ms:
public class Solution {
    public int lengthOfLastWord(String s) {
        final int len = s.length();
        StringBuilder lastWord = new StringBuilder();
        int count = len - 1;
        while (count >= 0)
        {
            if (s.charAt(count) == ' ' && lastWord.length() == 0)
            {
                --count;
                continue;
            }
            else if (s.charAt(count) == ' ' && lastWord.length() != 0)
            {
                break;
            }
            else
            {
                lastWord.append(s.charAt(count));
                --count;
            }
            
        }
        
        return lastWord.length();
    }
}

__________________
Task:
58. Length of Last Word

Given a string s consists of upper/lower-case alphabets and empty space characters ' ', return the length of last word in the string.

If the last word does not exist, return 0.

Note: A word is defined as a character sequence consists of non-space characters only.

For example, 
Given s = "Hello World",
return 5.