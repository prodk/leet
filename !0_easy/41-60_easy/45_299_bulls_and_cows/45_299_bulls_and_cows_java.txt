__________________
Testing:
0) "1807"
"7810"
"1A3B"
1) "1123"
"0111"
"1A1B"
2) "11231123112311231123112311231123"
"01110111011101110111011101110111"
"8A8B"
3) "0000000000111"
"1110000000111"
"10A0B"
4) "22233344445555"
"1110000000111"
"0A0B"
5) "0002223334111"
"1110000000111"
"3A3B"
6) "5552223334111"
"1110000000111"
"3A0B"
7) "5552223334111123451234512345123451234512345123451234512345123451234512345123451234512345123451234512345123451234512345"
"1110000000111543215432154321543215432154321543215432154321543215432154321543215432154321543215432154321543215432154321"
"24A84B"

__________________
Solutions:

0) use 2 historgrams: one for bulls, 2nd for all the numbers in the secret word, beats 51.98%, 5 ms:
public class Solution {
    public String getHint(String secret, String guess) {
        final Integer len = secret.length();
        if (len != guess.length())
        {
            return "0A0B";
        }
        
        int [] hist = new int[10];
        int [] histBulls = new int[10];
        
        Integer numOfBulls = 0;
        for (int i = 0; i < len; ++i)
        {
            final char c = secret.charAt(i);
            //System.out.println(c);
            ++hist[c - '0'];
            if (c == guess.charAt(i))
            {
                ++histBulls[c - '0'];
                ++numOfBulls;
            }
        }
        
        for (int i = 0; i < 10; ++i)
        {
            //System.out.println(i);
            if (hist[i] > 0)
            {
                hist[i] -= histBulls[i];
            }
        }
        
        Integer numOfCows = 0;
        for (int i = 0; i < len; ++i)
        {
            final char c = guess.charAt(i);
            if ((secret.charAt(i) != c) && (hist[c - '0'] > 0))
            {
                --hist[c - '0'];
                ++numOfCows;
            }
        }
        
        String res = numOfBulls.toString() + "A" + numOfCows.toString() + "B";
        return res;
    }
}

__________________
Task:
You are playing the following Bulls and Cows game with your friend: You write down a number and ask your friend to guess what the number is. Each time your friend makes a guess, you provide a hint that indicates how many digits in said guess match your secret number exactly in both digit and position (called "bulls") and how many digits match the secret number but locate in the wrong position (called "cows"). Your friend will use successive guesses and hints to eventually derive the secret number.

For example:

Secret number:  "1807"
Friend's guess: "7810"
Hint: 1 bull and 3 cows. (The bull is 8, the cows are 0, 1 and 7.)
Write a function to return a hint according to the secret number and friend's guess, use A to indicate the bulls and B to indicate the cows. In the above example, your function should return "1A3B".

Please note that both secret number and friend's guess may contain duplicate digits, for example:

Secret number:  "1123"
Friend's guess: "0111"
In this case, the 1st 1 in friend's guess is a bull, the 2nd or 3rd 1 is a cow, and your function should return "1A1B".
You may assume that the secret number and your friend's guess only contain digits, and their lengths are always equal.