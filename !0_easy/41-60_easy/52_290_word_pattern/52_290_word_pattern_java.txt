__________________
Testing:
0) "abba"
"dog cat cat dog"
out true
"dog cat cat cat"
out false
1) "aaaa"
"dog cat cat dog"
out false
"ab ab ab ab"
true
2) "aaaabbbbaaaa"
"ab ab ab ab cat cat cat cat ab ab ab ab"
out true
"aaaabbbbaaaa"
"ab ab ab ab cat cat cat cat ab ab ab a"
false

3) "aaabbbbaaa"
"ab ab ab ab cat cat cat cat ab ab ab ab"
false

4) "zaaabbbbaaaz"
"tralala ab ab ab ab cat cat cat cat ab ab ab ab tralala"
false

5) "zaaabbbbaaaz"
"tralala ab ab ab cat cat cat cat ab ab ab tralala"
true

6) "abbacddddcc"
"dog cat cat dog"
false
"dog cat cat dog fish horse horse horse horse fish fish"
true

7) "abcbacddddcc"
"dog cat fish cat dog fish horse horse horse horse fish fish"
true

__________________
Solutions:

0) beats 82.48 %, 2 ms, fastest:
public class Solution {
    public boolean wordPattern(String pattern, String str) {
        String [] words = str.split(" ");
        final int len = pattern.length();
        if (len != words.length)
        {
            return false;
        }
        
        int [] cp = new int[128];
        HashMap<String, Integer> cw = new HashMap<String, Integer>();
        for (int i = 0; i < len; ++i)
        {
            final char curChar = pattern.charAt(i);
            final String w = words[i];
            if (cp[curChar] == 0 && !cw.containsKey(w))
            {
                cp[curChar] = i + 1;
                cw.put(w, i + 1);
            }
            else
            {
                if (!cw.containsKey(w))
                {
                    return false;
                }
                
                if (cp[curChar] != cw.get(w))
                {
                    return false;
                }
            }
        }
        
        return true;
    }
}
__________________
Task:
290. Word Pattern

Given a pattern and a string str, find if str follows the same pattern.

Here follow means a full match, such that there is a bijection between a letter in pattern and a non-empty word in str.

Examples:
pattern = "abba", str = "dog cat cat dog" should return true.
pattern = "abba", str = "dog cat cat fish" should return false.
pattern = "aaaa", str = "dog cat cat dog" should return false.
pattern = "abba", str = "dog dog dog dog" should return false.
Notes:
You may assume pattern contains only lowercase letters, and str contains lowercase letters separated by a single space.