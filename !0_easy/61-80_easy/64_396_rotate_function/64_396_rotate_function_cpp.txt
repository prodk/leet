__________________
Testing:
0) [4, 3, 2, 6]
25;
1) [4, 3, 2, 6, -45, 2345, 48573, 1, 2, 3, 4, 5, 6, 0, -1234, 3456, 12, 3]
881994
2) [1, 2, 3, 4, 5, 6, 7, 8, 9, 10]
330
3) [-1, -2, -3, -4, -5, -6, -7, -8, -9, -10]
-205
4) [-100, -200, -300, -400, -500, -600, -700, -8, -9, -10]
-8764


__________________
Solutions:

0) 16 ms:
class Solution {
public:
    int maxRotateFunction(vector<int>& A) {
        const int len = A.size();
        if (0 == len)
        {
            return 0;
        }
        
        int sum = 0;
        int fprev = 0;
        for (int i = 0; i < len; ++i)
        {
            sum += A[i];
            fprev += i*A[i];
        }
        
        int fmax = fprev;
        for (int k = 1; k < len; ++k)
        {
            const int fcur = fprev + sum - len*A[len-k];
            fmax = std::max<>(fmax, fcur);
            fprev = fcur;
        }
        
        return fmax;
    }
};

__________________
Task:
396. Rotate Function

Given an array of integers A and let n to be its length.

Assume Bk to be an array obtained by rotating the array A k positions clock-wise, we define a "rotation function" F on A as follow:

F(k) = 0 * Bk[0] + 1 * Bk[1] + ... + (n-1) * Bk[n-1].

Calculate the maximum value of F(0), F(1), ..., F(n-1).

Note:
n is guaranteed to be less than 105.

Example:

A = [4, 3, 2, 6]

F(0) = (0 * 4) + (1 * 3) + (2 * 2) + (3 * 6) = 0 + 3 + 4 + 18 = 25
F(1) = (0 * 6) + (1 * 4) + (2 * 3) + (3 * 2) = 0 + 4 + 6 + 6 = 16
F(2) = (0 * 2) + (1 * 6) + (2 * 4) + (3 * 3) = 0 + 6 + 8 + 9 = 23
F(3) = (0 * 3) + (1 * 2) + (2 * 6) + (3 * 4) = 0 + 2 + 12 + 12 = 26

So the maximum value of F(0), F(1), F(2), F(3) is F(3) = 26.
