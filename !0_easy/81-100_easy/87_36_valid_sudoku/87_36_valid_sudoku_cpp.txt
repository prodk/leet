__________________
Testing:
0) [".87654321","2........","3........","4........","5........","6........","7........","8........","9........"]
true
1) [".87654321","2........","3.......1","4........","5........","6........","7........","8........","9........"]
false
__________________
Solutions:
1) beats 36.97 %, 16 ms:
class Solution {
public:
    bool isValidSudoku(vector<vector<char>>& board) {
        bool result = checkRows(board);
        result = result && checkColumns(board);
        result = result && checkAreas(board);
        
        return result;
    }
    
private:
    bool checkRows(const vector<vector<char>>& board) const
    {
        const int nRows = board.size();
        for (int i = 0; i < nRows; ++i)
        {
            int hist[10] = {0};
            for (int j = 0; j < nRows; ++j)
            {
                const char c = board[i][j];
                if (c != '.')
                {
                    ++hist[c - '0'];
                }
            }
            
            for (int k = 0; k < 10; ++k)
            {
                if (hist[k] > 1)
                {
                    //cout << "row" << (char)(k + '0') << endl;
                    return false;
                }
            }
        }
        
        return true;
    }
    
    bool checkColumns(const vector<vector<char>>& board) const
    {
        const int nRows = board.size();
        for (int i = 0; i < nRows; ++i)
        {
            int hist[10] = {0};
            for (int j = 0; j < nRows; ++j)
            {
                const char c = board[j][i];
                if (c != '.')
                {
                    ++hist[c - '0'];
                }
            }
            
            for (int k = 0; k < 10; ++k)
            {
                if (hist[k] > 1)
                {
                    //cout << "col " << (char)(k + '0') << endl;
                    return false;
                }
            }
        }
        
        return true;
    }
    
    bool checkAreas(const vector<vector<char>>& board) const
    {
        for (int i = 1; i < 9; i += 3)
        {
            for (int j = 1; j < 9; j += 3)
            {
                int hist[10] = {0};
                for (int k = -1; k < 2; ++k)
                {
                    for (int n = -1; n < 2; ++n)
                    {
                        //cout << board[i+k][j+n] << " ";
                        const char c = board[i+k][j+n];
                        if (c != '.')
                        {
                            ++hist[c - '0'];
                        }
                    }
                    //cout << endl;
                }
                
                for (int x = 0; x < 10; ++x)
                {
                    if (hist[x] > 1)
                    {
                        //cout << "area " << x << " " << hist[x] << " " << (char)(x + '0') << endl;
                        return false;
                    }
                }
            }
        }
        
        return true;
    }
};

0) transpose wrong, beats %, ms:
class Solution {
public:
    bool isValidSudoku(vector<vector<char>>& board) {
        bool result = checkRows(board);
        if (result)
        {
            const vector<vector<char>> transposed = transpose(board);
            result = checkRows(transposed);
            result = result && checkAreas(board);
        }
        
        return result;
    }
    
private:
    bool checkRows(const vector<vector<char>>& board) const
    {
        const int nRows = board.size();
        for (int i = 0; i < nRows; ++i)
        {
            int hist[10] = {0};
            for (int j = 0; j < nRows; ++j)
            {
                const char c = board[i][j];
                if (c != '.')
                {
                    ++hist[c - '0'];
                }
            }
            
            for (int k = 0; k < 10; ++k)
            {
                if (hist[i] > 1)
                {
                    return false;
                }
            }
        }
        
        return true;
    }
    
    const vector<vector<char>> transpose(const vector<vector<char>>& board) const
    {
        const int nRows = 9;
        vector<vector<char>> res;
        res.resize(nRows);
        for (int i = 0; i < nRows; ++i)
        {
            for (int j = 0; j < nRows; ++j)
            {
                res[j].push_back(board[i][j]);
            }
        }
        
        return res;
    }
    
    bool checkAreas(const vector<vector<char>>& board) const
    {
        for (int i = 1; i < 9; i += 3)
        {
            int hist[10] = {0};
            for (int j = 1; j < 9; j += 3)
            {
                for (int k = -1; k < 2; ++k)
                {
                    for (int n = -1; n < 2; ++n)
                    {
                        const char c = board[i+k][j+n];
                        if (c != '.')
                        {
                            ++hist[c - '0'];
                        }
                    }
                }
            }
            
            for (int i = 0; i < 10; ++i)
            {
                if (hist[i] > 1)
                {
                    return false;
                }
            }
        }
        
        return true;
    }
};

__________________
Task:
36. Valid Sudoku

Determine if a Sudoku is valid, according to: Sudoku Puzzles - The Rules.

The Sudoku board could be partially filled, where empty cells are filled with the character '.'.


A partially filled sudoku which is valid.

Note:
A valid Sudoku board (partially filled) is not necessarily solvable. Only the filled cells need to be validated.