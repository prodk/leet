__________________
Testing:

__________________
Solutions:

0) beats %, ms:
/**
 * Definition for a binary tree node.
 * struct TreeNode {
 *     int val;
 *     TreeNode *left;
 *     TreeNode *right;
 *     TreeNode(int x) : val(x), left(NULL), right(NULL) {}
 * };
 */
class Solution {
public:
   int sumOfLeftLeaves(TreeNode* root) {
      return leftSum(root, false);
   }
private:
   int leftSum(TreeNode* root, bool isLeft) const
   {
      if (0 == root)
      {
         return 0;
      }

      int sum = leftSum(root->left, true) + leftSum(root->right, false);

      if (isLeaf(root) && isLeft)
      {
         sum += root->val;
      }

      return sum;
   }
   
   bool isLeaf(TreeNode* root) const
   {
       return (0 != root) && (0 == root->left) && (0 == root->right);
   }
};

__________________
Task:
404. Sum of Left Leaves

Find the sum of all left leaves in a given binary tree.

Example:

    3
   / \
  9  20
    /  \
   15   7

There are two left leaves in the binary tree, with values 9 and 15 respectively. Return 24.
