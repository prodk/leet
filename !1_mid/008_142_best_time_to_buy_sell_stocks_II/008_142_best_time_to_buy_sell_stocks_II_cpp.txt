__________________
Testing:
0) []
0
1) [1, 2, 3, 4, 5, 6, 7]
6
2) 	[1, 1, 1,1,1,1,1,1]
0
3) [-1, -1, -1,-1,-1,-1,-1,1]
2
4) [1, -20, -12, 123, 23, 0, 3, 4, -45, -17, -100, 13, 77, 89, 26, -5, -1, 2, 3,4, 5, -1, -2, -3, 89]
466


__________________
Solutions:

0) beats %, ms:
class Solution {
public:
    int maxProfit(vector<int>& prices) {
        const int len = prices.size();
        int maxProfit = 0;
        
        for (int i = 1; i < len; ++i)
        {
            if (prices[i] > prices[i-1]) maxProfit += prices[i] - prices[i-1];
        }
        
        return maxProfit;
    }
};

__________________
Task:
122. Best Time to Buy and Sell Stock II

Say you have an array for which the ith element is the price of a given stock on day i.

Design an algorithm to find the maximum profit. 
You may complete as many transactions as you like (ie, buy one and sell one share of the stock multiple times). 
However, you may not engage in multiple transactions at the same time (ie, you must sell the stock before you buy again).