__________________
Testing:
0) [0]
1
1) [1, 3, 2]
2
2) [0, 1, 2, 3, 6, 5, 4, 7, 9, 10, 8]
11
3) [1, 2, 3, 6, 5, 4, 7, 9, 10, 8]
0
4) []
0
5) [0, 1, 3, 6, 5, 4, 7, 9, 10, 8]
2
6) [2]
3
7) [2,3]
2
8) [1,2,3]
0
9) [1, 3, 0]
2
10) [9,6,4,2,3,5,7,0,1]
8


__________________
Solutions:
1) O(n) space and time, beats
int missingNumber(vector<int>& nums) {
        nums.push_back(-1);
    for (int i = 0 ;i < nums.size(); i++)
        while (i != nums[i] && nums[i] != -1)
            swap(nums[i], nums[nums[i]]);
    for (int i = 0; i < nums.size(); i++)
        if (nums[i] == -1)
            return i;
    return 0;

0) O(nlogn) beats 6.44 %, 63 ms:
class Solution {
public:
    int missingNumber(vector<int>& nums) {
        sort(nums.begin(), nums.end());
        
        const int len = nums.size();
        if (len == 0) return 0;
        
        int res = -1;
        for (int i = 1; i < len; ++i)
        {
            if (nums[i] - nums[i-1] != 1)
            {
                res = nums[i-1] + 1;
            }
        }
        
        if (res == -1)
        {
            res = (nums[0] != 0) ? 0 : nums[len-1] + 1;
        }
        
        return res;
    }
};

__________________
Task:
268. Missing Number

Given an array containing n distinct numbers taken from 0, 1, 2, ..., n, find the one that is missing from the array.

For example,
Given nums = [0, 1, 3] return 2.

Note:
Your algorithm should run in linear runtime complexity. Could you implement it using only constant extra space complexity?

